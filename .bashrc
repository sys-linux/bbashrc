# If shell is not interactive: Bail
[[ $- != *i* ]] && return

self=${BASH_SOURCE[0]}
dir_self_nofoll=$(realpath $(dirname "${self}"))
dir_self_follow=$(dirname "$(readlink -m "${self}")")
sources_dir_possibilities=("${HOME}/.bash_setup" "${HOME}/bbashrc/.bash_setup" "${HOME}/devel/bbashrc/.bash_setup" "${dir_self_nofoll}/.bash_setup" "${dir_self_follow}/.bash_setup" ".bash_setup")

# Include files containing settings, aliases etc.
found_sources_dir=""
for sources_dir in "${sources_dir_possibilities[@]}"; do
    if [[ -d "${sources_dir}" ]]; then
	    for fl in "${sources_dir}"/*; do
		    if [[ -f $fl ]]; then
			    source $fl
		    fi
	    done
	    if [[ -d "${sources_dir}/config" ]]; then
            export bash_config_dir="${sources_dir}/config"
        fi
        found_sources_dir=True
        break
    fi
done
if [[ ! ${found_sources_dir} ]]; then
    echo "WARNING: Bash setup directory not found at any of these locations: ${sources_dir_possibilities[*]}"
fi
