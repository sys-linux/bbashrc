#/usr/bin/env bash

check-aliases()
# Check if aliases are (accidentally) covering commands or functions
{
    # As an argument, the function can receive the path to a file containing aliases
    #  to be excluded from the check. One alias per line, lines can be commented out (by '#')
    local ignored=()
    if [[ ${1} ]]; then
        ignored=($(sed -E 's/#.*//g' "${1}"))
    fi

    local covering_aliases=($(covering-aliases))

    local alias found_invalid
    # Go through found covering aliases
    for alias in "${covering_aliases[@]}"; do
        # If alias is in ignore list: Ignore it
        if [[ " ${ignored[*]} " =~ " ${alias} " ]]; then
            continue
        fi
        # Print a warning for the covering alias
        echo "WARNING: Alias '${alias}' is covering up a function or command."
        # Acknowledge presence of covering alias
        found_invalid=True
    done
    if [[ ${found_invalid} ]]; then
        echo "To ignore all alias warnings, run 'alias-ignore-covering'."
        echo "To ignore the alias warning for a specific alias, run 'alias-ignore-covering <alias>'."
    fi

    return 0
}

covering-aliases()
# Print aliases that cover commands of functions, one in each line
{
    local aliases functions
    # Get array of currently active aliases (each in format "alias <name>='<substitution>'")
    readarray -t aliases <<< $(alias -p)
    # Get array of names of currently declared bash functions
    functions=($(sed -e 's/declare -f//g' <<< $(declare -F)))

    local entry name subst_main status
    # Go through aliases and check for each of them if it could be problematic
    for entry in "${aliases[@]}"; do
        # Name of the alias
        name=$(awk "{ sub(/[^ ]* /, \"\"); sub(/[=].*/, \"\"); print }" <<< "${entry}")
        # Main command of the substitution, e.g. 'ls' if the alias is "alias l='ls -a'
        subst_main=$(awk "{ sub(/[^']*'/, \"\"); sub(/[^a-zA-Z0-9\-_\.].*/, \"\"); print }" <<< "${entry}")
        # Allow aliases to have the same name as a command if they refer to this command
        if [[ "${name}" == "${subst_main}" ]]; then
            continue
        fi
        # Check if there is a command (binary / script) in PATH with the same name as the alias
        which --skip-functions --skip-alias "$name" &> /dev/null
        status=$?
        # If a command was found in PATH: Print alias
        if [[ status -eq 0 ]]; then
            echo "${name}"
        # If there is a function with the same name as the alias: Print alias
        elif [[ " ${functions[*]} " =~ " ${name} " ]]; then
            echo "${name}"
        fi
    done
    return 0
}

check-functions()
# Check if functions are (accidentally) covering commands
{
    # As an argument, the function can receive the path to a file containing functions
    #  to be excluded from the check. One function per line, lines can be commented out (by '#')
    local ignored=()
    if [[ ${1} ]]; then
        ignored=($(sed -E 's/#.*//g' "${1}"))
    fi

    local covering_functions=($(covering-functions))

    local func found_invalid
    # Go through found covering functions
    for func in "${covering_functions[@]}"; do
        # If function is in ignore list: Ignore it
        if [[ " ${ignored[*]} " =~ " ${func} " ]]; then
            continue
        fi
        # Print a warning for the covering function
        echo "WARNING: Function '${func}' is covering up a command."
        # Acknowledge presence of covering function
        found_invalid=True
    done
    if [[ ${found_invalid} ]]; then
        echo "To ignore all function warnings, run 'function-ignore-covering'."
        echo "To ignore the function warning for a specific function, run 'function-ignore-covering <function>'."
    fi

    return 0
}

covering-functions()
# Print functions that cover commands, one in each line
{
    # Get array of names of currently declared bash functions
    local functions=($(sed -e 's/declare -f//g' <<< $(declare -F)))

    local name status
    # Go through functions and check for each of them if it could be problematic
    for name in "${functions[@]}"; do
        # Check if there is a command (binary / script) in PATH with the same name as the function
        which --skip-functions --skip-alias "$name" &> /dev/null
        status=$?
        # If a command was found in PATH: Print a warning
        if [[ status -eq 0 ]]; then
            echo "${name}"
        fi
    done
    return 0
}

path-invalid-dirs()
# Print directories in PATH that don't exist, one in each line
{
    local directories
    # Get array of directories in PATH
    readarray -t -d ":" directories <<< $PATH

    local directory
    # Go through directories in PATH and check for each of them if it exists
    for directory in "${directories[@]}"; do
        readarray -t directory <<< $directory
        # Adjust the directory string in case of some anomalies (esp. newlines)
        directory=$(sed -E 's/(^ *)|( *$)//g' <<< "${directory[@]}")
        # If directory does not exist: Print a warning
        if [[ ! -d "${directory}" ]]; then
            echo "${directory}"
        fi
    done
    return 0
}

check-path()
# Check if directories in PATH exist
{
    # As an argument, the function can receive the path to a file containing directories
    #  to be excluded from the check. One directory per line, lines can be commented out (by '#')
    local ignored=()
    if [[ ${1} ]]; then
        ignored=($(sed -E 's/#.*//g' "${1}"))
    fi

    local directories=($(path-invalid-dirs))

    local directory found_invalid
    # Go through invalid directories in PATH
    for directory in "${directories[@]}"; do
        # If directory is in ignore list: Ignore it
        if [[ " ${ignored[*]} " =~ " ${directory} " ]]; then
            continue
        fi
        # Print a warning for the invalid directory
        echo "WARNING: Path entry '${directory}' is not a valid directory"
        # Acknowledge presence of invalid directory
        found_invalid=True
    done
    if [[ ${found_invalid} ]]; then
        echo "To ignore all path warnings, run 'path-ignore-invalid'."
        echo "To ignore the path warning for a specific directory, run 'path-ignore-invalid <directory>'."
    fi

    return 0
}

path-ignore-invalid()
# Add all or one specific directory to the list of directories to be ignored when checking the validity of PATH
{
    if [[ -d "${bash_config_dir}" ]]; then
        file_ignored_invalid_path_entries="${bash_config_dir}/ignored_invalid_path_entries"
    else
        echo "ERROR: config directory not found"
        return 1
    fi

    # If argument is given, it is the directory to be added to ignore list.
    # Else, add all currently invalid directories.
    if [[ ${1} ]]; then
        directories=(${1})
    else
        directories=($(path-invalid-dirs))
    fi

    for directory in "${directories[@]}"; do
        # If directory is already in ignore list: Don't add it again
        if [[ -f "${file_ignored_invalid_path_entries}" ]] && [[ $(grep "^${directory}$" "${file_ignored_invalid_path_entries}") ]]; then
            continue
        fi
        # Add directory to ignore list
        echo "${directory}" >> "${file_ignored_invalid_path_entries}"
    done

    return 0
}

alias-ignore-covering()
# Add all or one specific alias to the list of aliases to be ignored when checking for aliases covering functions and commands
{
    if [[ -d "${bash_config_dir}" ]]; then
        file_ignored_covering_aliases="${bash_config_dir}/ignored_covering_aliases"
    else
        echo "ERROR: config directory not found"
        return 1
    fi

    # If argument is given, it is the alias to be added to ignore list.
    # Else, add all currently covering aliases.
    if [[ ${1} ]]; then
        aliases=(${1})
    else
        aliases=($(covering-aliases))
    fi

    for alias in "${aliases[@]}"; do
        # If alias is already in ignore list: Don't add it again
        if [[ -f "${file_ignored_covering_aliases}" ]] && [[ $(grep "^${alias}$" "${file_ignored_covering_aliases}") ]]; then
            continue
        fi
        # Add alias to ignore list
        echo "${alias}" >> "${file_ignored_covering_aliases}"
    done

    return 0
}

function-ignore-covering()
# Add all or one specific function to the list of functions to be ignored when checking for functions covering commands
{
    if [[ -d "${bash_config_dir}" ]]; then
        file_ignored_covering_functions="${bash_config_dir}/ignored_covering_functions"
    else
        echo "ERROR: config directory not found"
        return 1
    fi

    # If argument is given, it is the function to be added to ignore list.
    # Else, add all currently covering functions.
    if [[ ${1} ]]; then
        functions=(${1})
    else
        functions=($(covering-functions))
    fi

    for func in "${functions[@]}"; do
        # If function is already in ignore list: Don't add it again
        if [[ -f "${file_ignored_covering_functions}" ]] && [[ $(grep "^${fun}$" "${file_ignored_covering_functions}") ]]; then
            continue
        fi
        # Add function to ignore list
        echo "${func}" >> "${file_ignored_covering_functions}"
    done

    return 0
}

path-add()
# Add directory to PATH
{
    if [[ ! $1 ]]; then
        echo "Usage: path-add <newdir>"
        return 1
    fi
    local newdir=${1}
    newdir=$(realpath "${newdir}")
    if [[ "${newdir}" =~ ":" ]]; then
        echo "Directories in path must not contain ':'. Aborting."
        return 1
    fi
    PATH="${newdir}:${PATH}"
    return 0
}

path-remove()
# Remove directory from PATH
{
    if [[ ! $1 ]]; then
        echo "Usage: path-remove <dir>"
        return 1
    fi
    local remdir=${1}
    remdir=$(realpath "${remdir}")
    local directories
    # Get array of directories in PATH
    readarray -t -d ":" directories <<< $PATH

    local directory num_found
    # Go through directories in PATH and check for each of them if it should be removed
    for directory in "${directories[@]}"; do
        # If directory matches the directory to be removed: Remove it, increase removal counter
        if [[ "${directory}" -ef "${remdir}" ]]; then
            PATH=${PATH/${directory}/}
            ((num_found++))
        fi
    done
    # Clean up PATH: Remove consecutive colons and colons at beginning and end of PATH
    PATH=$(sed -E 's/:{2,}/:/g ; s/(^:)|(:$)//g' <<< "$PATH")
    # Print information about what has been done
    if [[ "${num_found}" -eq 1 ]]; then
        echo "Removed '${remdir}' from PATH"
        return 0
    elif [[ "${num_found}" -gt 1 ]]; then
        echo "Removed '${remdir}' from PATH ${num_found} times"
        return 0
    else
        echo "Did not find '${remdir}' in PATH"
        return 2
    fi
    return 0
}

opts()
# NOT A USEFUL FUNCTION BUT JUST FOR EXPERIMENTING WITH ARGUMENTS
{
    local OPTIND OPTARG option
    local opt_a opt_b opt_c
    while getopts "ab:c" option "$@"; do
        echo "Option '${option}' with value '${OPTARG}'"
    done
    local arg1_index arg2_index arg1 arg2
    arg1_index=$(($#-1))
    arg2_index=$#
    arg1=${!arg1_index}
    arg2=${!arg2_index}
    echo "Arg1: ${arg1}"
    echo "Arg2: ${arg2}"
}
