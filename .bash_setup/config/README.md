This directory is meant to contain user-specific configuration files that should not be included in the repository.
This file should keep the directory itself in the repository.
