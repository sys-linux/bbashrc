# bbashrc



Add some convenience features to the bash shell through a *.bashrc* file that sources some more files for better maintainability.

## Installation

To utilize the bash modifications, the *.bashrc* file has to be located in your home directory, and if the inclusion of the other resources should work without further intervention, the *.bash_setup* directory has to be located in your home directory as well.

This can be achieved by simply moving both the file and the directory to the home directory, OR, in order to be able to update the project from the repository without manually intervening every time, you can store (clone) it at an arbitrary location and symlink *.bashrc* and *.bash_setup* to it from your home directory.
